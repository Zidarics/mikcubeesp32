# MikCube ESP32 application

## Welcome on MikCube project of Department of Automation of Institute of Information and Electrical  of Faculty of Engineering and Information Technology of University of Pecs. 

This project implements the control of an 8x8x8 LED cube. The project includes a hardware cube implemented in hardware that can display 8 pre-programmed figures. The cube has a Bluetooth interface through which you can switch between figures.
Another element of the project is an Android application that can model the physical cube and communicates with the physical cube via the bluetooth interface.
The application allows you to build an animation from up to 100 "layers". These animations can be saved and shared.
The source codes for the project are public for both ESP32 and Android. 
Our goal is that, to give students an exciting game that they can improve on, the completed animations can be shared with each other and with us.
The best animations, if submitted to us, will be made publicly available.
If anyone has either the ESP32 or Android application If you change the code and submit it, it is merged and the person's name is displayed below the contact

### Project sources
- [ESP32] (https://gitlab.com/Zidarics/mikcubeesp32) 
- [Android] (https://gitlab.com/Zidarics/mikcubeandroid) 

### A projekt creators:

#### Contributor persons:
* Zidarics Zoltán (ESP32, Android)
* Alexander Novopashin (OpenGL)
* Brenner Csaba (Hardware planning, building)
Tóth Márió (3D printing)
Varga Zsolt (3D printing)

#### Contributor firms:
Energotest Kft. (3D printing)
Brenner Elektronika Kft. (Hardware)

## A Pécsi Tudományegyetem Műszaki és informatikai Kar Automatizálási tanszék MikCube projektje. 
A projekt egy 8x8x8 elemű LED kocka vezérlését valósítja meg. A projekt tartalmaz egy hardware-ben megvalósított fizikai kockát, amely 8 előre programozott ábrát tud megjeleníteni. A kockának van Bluetooth interfésze, amelyen keresztül lehet az ábrák között kapcsolni.
A projekt másik eleme egy Android applikáció, amely a fizikai kockát tudja modellezni, ill. a bluetooth interface-en keresztül kommunikál a fizikai kockával. 
Az applikáció lehetővé teszi, hogy max 100 "rétegből" lehessen felépíteni egy animációt. Ezek az animációk elmenthetők és megoszthatók. 
A projekt forráskódjai nyilvánosak mind az ESP32-é mind az Android-é is. Ezzel az a célunk, hogy a hallgatóknak adjunk egy izgalmas játékot, amelyet továbbfejleszthetnek, az
elkészült animációkat egymással és velünk is megoszthatják. A legjobb animációkat, ha beküldik nekünk, akkor nyilvánosan elérhetővé tesszük. Ha valaki akár az ESP32 akár az Android applikáció
kódját módosítja és beküldi, akkor beolvasztjuk és a névjegy alatt az illető nevét feltüntetjük. 

### A projekt forráskódja
- [ESP32] (https://gitlab.com/Zidarics/mikcubeesp32) 
- [Android] (https://gitlab.com/Zidarics/mikcubeandroid) 
 
### A projekt alkotói:

#### Közreműködők:
Zidarics Zoltán (ESP32, Android)
Alexander Novopashin (OpenGL)
Brenner Csaba (Hardware tervezés, kivitelezés)
Tóth Márió (3D nyomtatás)
Varga Zsolt (3D nyomtatás)

Közreműködő cégek:
Energotest Kft. (3D nyomtatás)
Brenner Elektronika Kft. (Hardware)

