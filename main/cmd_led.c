/**
 *  Console for LED
 */

#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "linenoise/linenoise.h"
#include "argtable3/argtable3.h"
#include "esp_console.h"
#include "led.h"
#include "esp_log.h"
#include "led_strip.h"
#include <regex.h>

#define TAG_CMD_LED "CMD_LED"

#undef DEBUG

#ifdef DEBUG
#define DEB(fmt, ...)   DEB( fmt, ##__VA_ARGS__)
#else
#define DEB(fmt, ...)
#endif

#define SWITCH_ON "on"
#define SWITCH_OFF "off"

static int get_current(int argc, char **argv) {
	printf("Current figure is: %d\n", led_get_current_figure());
	return 0;
}

static void register_get_current() {
	const esp_console_cmd_t cmd = {
			.command = "getcurrent",
			.help = "Get current figure displayed",
			.hint = NULL,
			.func = &get_current };

	ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}

static struct {
	struct arg_rex *on_off;
	struct arg_end *end;
} set_display_args;


static int set_display(int argc, char **argv) {
	int nerrors = arg_parse(argc, argv, (void**) &set_display_args);

	if (nerrors || argc==1) {
		arg_print_errors(stderr, set_display_args.end, argv[0]);
		return 1;
	}

	if (set_display_args.on_off->count>0) {
		if (strcasecmp(set_display_args.on_off->sval[0], SWITCH_ON)==0) {
			led_set_display(1);
			ESP_LOGI(TAG_CMD_LED, "display is on");
			return 0;
		}
		if (strcasecmp(set_display_args.on_off->sval[0], SWITCH_OFF)==0) {
			led_set_display(0);
			ESP_LOGI(TAG_CMD_LED, "display is off");
			return 0;
		}
	}
	arg_print_errors(stderr, set_display_args.end, argv[0]);
	return 1;
}

static void register_set_display() {
	set_display_args.on_off = arg_rex0(NULL, NULL, ".+", "display status", REG_ICASE, "on|off");
	set_display_args.end = arg_end(2);

	const esp_console_cmd_t cmd = {
			.command = "display",
			.help = "Switch off/on display.\n",
			.hint = NULL,
			.func = &set_display
	};
	ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}

static struct {
	struct arg_int *index;
	struct arg_end *end;
} set_current_args;

static int set_current(int argc, char **argv) {
	int nerrors = arg_parse(argc, argv, (void**) &set_current_args);

	if (nerrors || argc==1) {
		arg_print_errors(stderr, set_current_args.end, argv[0]);
		return 1;
	}

	if (set_current_args.index->count) {
		uint c = set_current_args.index->ival[0];
		if (c>=PF_UNKNOWN) {
			printf("Maximum value is %d\n", PF_UNKNOWN-1);
			return 1;
		}
		if (led_set_current_figure(c) == EXIT_SUCCESS) {
			ESP_LOGI(TAG_CMD_LED, "Figure changed to %d", c);
			return 0;
		}
		arg_print_errors(stderr, set_current_args.end, argv[0]);
		return 1;
	}

	arg_print_errors(stderr, set_current_args.end, argv[0]);
	return 1;
}

static void register_set_current() {
	set_current_args.index = arg_intn(NULL, NULL, "<index>", 0, PF_UNKNOWN-1, "current index");
	set_current_args.end = arg_end(2);

	const esp_console_cmd_t cmd = {
			.command = "setcurrent",
			.help = "Set current figure.\n"
					"  Example:\n"
					"    setcurrent 0\n",
			.hint = NULL,
			.func = &set_current
	};
	ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}

static int get_default_color(int argc, char **argv) {
	struct led_color_t color = led_get_default_color();
	printf("Default color is #%02x%02x%02x\n", color.red, color.green, color.blue);
	return 0;
}

static void register_get_default_color() {
	const esp_console_cmd_t cmd = {
			.command = "getcolor",
			.help = "Get default color",
			.hint = NULL,
			.func = &get_default_color};
	ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}

static struct {
	struct arg_rex *color;
	struct arg_end *end;
} set_color_args;


static inline void parse_color(const char *str, struct led_color_t *color) {
	int red,green,blue;
	sscanf(str, "#%02x%02x%02x", &red, &green, &blue);
	color->red=(uint8_t)red;
	color->green=(uint8_t)green;
	color->blue=(uint8_t)blue;
}

static int set_default_color(int argc, char **argv) {
	int nerrors = arg_parse(argc, argv, (void**) &set_color_args);
	DEB( "nerrors:%d", nerrors);

	if (nerrors) {
		arg_print_errors(stderr, set_color_args.end, argv[0]);
		return 1;
	}

	if (set_color_args.color->count) {
		const char *cstr = set_color_args.color->sval[0];
		struct led_color_t color;
		parse_color(cstr, &color);
		led_set_default_color(&color);
		printf("default color is set to #%02x%02x%02x\n", color.red, color.green, color.blue);
		return 0;
	}
	return 1;
}

static void register_set_default_color() {
	set_color_args.color = arg_rex0(NULL, NULL, "#[0-9a-fA-F]+","default color", REG_ICASE, "default color");
	set_color_args.end = arg_end(2);

	const esp_console_cmd_t cmd = {
			.command = "setcolor",
			.help = "Set default color.\n"
					"  Example:\n"
					"    setcolor #804220\n",
			.hint = NULL,
			.func = &set_default_color
	};
	ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));

}

static struct {
   struct arg_int *hplain;
   struct arg_int *vplain;
   struct arg_int *row;
   struct arg_int *col;
   struct arg_rex *color;
   struct arg_int *index;
   struct arg_end *end;
} set_pixel_args;

static int set_pixel(int argc, char **argv) {
	int nerrors = arg_parse(argc, argv, (void**) &set_pixel_args);
	DEB( "nerrors:%d", nerrors);

	if (nerrors) {
		arg_print_errors(stderr, set_pixel_args.end, argv[0]);
		return 1;
	}

	int hplain=-1;
	int vplain=-1;
	int row=-1;
	int col=-1;
	int index=-1;
	struct led_color_t color = led_get_default_color();

	if (set_pixel_args.hplain->count>0) {
		hplain = set_pixel_args.hplain->ival[0];
		DEB( "hplain:%d", hplain);
		if (hplain<0 || hplain>=LED_PLAINS) {
			printf("value must be between 0 up to %d", LED_PLAINS);
			return 1;
		}
	}

	if (set_pixel_args.vplain->count>0) {
		vplain = set_pixel_args.vplain->ival[0];
		DEB( "vplain:%d", vplain);
		if (vplain<0 || vplain>=LED_PLAINS) {
			printf("value must be between 0 up to %d", LED_PLAINS);
			return 1;
		}
	}

	if (set_pixel_args.row->count>0) {
		row = set_pixel_args.row->ival[0];
		DEB( "row:%d", row);
		if (row<0||row>=LED_ROWS) {
			printf("value must be between 0 up to %d", LED_ROWS);
			return 1;
		}
	}

	if (set_pixel_args.col->count>0) {
		col = set_pixel_args.col->ival[0];
		DEB( "col:%d", col);
		if (col<0||col>=LED_COLUMNS) {
			printf("value must be between 0 up to %d", LED_COLUMNS);
			return 1;
		}
	}

	if (set_pixel_args.index->count>0) {
		index = set_pixel_args.index->ival[0];
		DEB( "index:%d", index);
		if (index<0||index>=LED_COLUMNS*LED_ROWS*LED_PLAINS) {
			printf("value must be between 0 up to %d", LED_COLUMNS*LED_ROWS*LED_PLAINS);
			return 1;
		}
	}

	if (set_pixel_args.color->count>0) {
		parse_color(set_pixel_args.color->sval[0], &color);
		DEB( "color: %02x%02x%02x", color.red, color.green, color.blue);
	}

	if (index >=0 && (hplain>=0 || vplain>=0)) {
		printf("If index is defined hplain or vplain not valid\n");
		return 1;
	}

	if (hplain >=0 && vplain>=0) {
		printf("both hplain & vplain defined\n");
		return 1;
	}

	if (index!=-1) {
		if (led_set_absolute_pixel(index, &color)==EXIT_SUCCESS) {
			led_show();
			ESP_LOGI(TAG_CMD_LED, "set absolute %d to #%02x%02x%02x", index, color.red, color.green, color.blue);
			return 0;
		}
		arg_print_errors(stderr, set_pixel_args.end, argv[0]);
		return 1;
	}

	if (hplain>=0) {
		if (row>=0 && col>=0 && led_set_plain_pixel(P_HORIZONTAL, hplain, row, col, &color)==EXIT_SUCCESS) {
			led_show();
			ESP_LOGI(TAG_CMD_LED, "Set horizontal plain %d row %d col %d to #%02x%02x%02x", hplain, row, col, color.red, color.green, color.blue);
			return 0;
		}

		arg_print_errors(stderr, set_pixel_args.end, argv[0]);
		return 1;
	}

	if (vplain>=0) {
		if (row>=0 && col>=0 && led_set_plain_pixel(P_VERTICAL, vplain, row, col, &color)==EXIT_SUCCESS) {
			led_show();
			ESP_LOGI(TAG_CMD_LED, "Set vertical plain %d row %d col %d to #%02x%02x%02x", vplain, row, col, color.red, color.green, color.blue);
			return 0;
		}

		arg_print_errors(stderr, set_pixel_args.end, argv[0]);
		return 1;
	}
	arg_print_errors(stderr, set_pixel_args.end, argv[0]);
	return 1;
}

static void register_set_pixel() {
	set_pixel_args.hplain = arg_int0("h", "hplain", "<plain>", "horizontal plain index");
	set_pixel_args.vplain = arg_int0("v", "vplain", "<plain>", "vertical plain index");
	set_pixel_args.row = arg_int0("r", "row", "<row>", "row index");
	set_pixel_args.col = arg_int0("c", "column", "<column>", "column index");
	set_pixel_args.color = arg_rex0("o", "color", "#[0-9a-fA-F]+","#RGB color", REG_ICASE, "color");
	set_pixel_args.index = arg_int0("i", "index", "<index>", "absolute index");
	set_pixel_args.end = arg_end(2);
	const esp_console_cmd_t cmd = {
		.command = "setpixel",
		.help = "Set pixel.\n"
				" Example:\n"
				"   setpixel --vplain 1 --row 3 --column 2 --color #424242\n"
				"   setpixel --hplain 1 -r 3 -c 2 -o #424242\n"
				"   setpixel --index 42 -o #424242\n"
				"   if -o is omitted it will use the default color. See setcolor command\n",
		.hint = NULL,
		.func = &set_pixel
	};

	ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}

static struct {
   struct arg_int *hplain;
   struct arg_int *vplain;
   struct arg_int *center_x;
   struct arg_int *center_y;
   struct arg_int *radius;
   struct arg_rex *color;
   struct arg_end *end;
} draw_circle_args;

static int draw_circle(int argc, char **argv) {
	int nerrors = arg_parse(argc, argv, (void**) &draw_circle_args);
	DEB( "nerrors:%d", nerrors);

	if (nerrors) {
		arg_print_errors(stderr, draw_circle_args.end, argv[0]);
		return 1;
	}

	int hplain=-1;
	int vplain=-1;
	int centerx=-1;
	int centery=-1;
	int radius=-1;

	struct led_color_t color = led_get_default_color();

	if (draw_circle_args.hplain->count>0) {
		hplain = draw_circle_args.hplain->ival[0];
		DEB( "hplain:%d", hplain);
		if (hplain<0 || hplain>=LED_PLAINS) {
			printf("value must be between 0 up to %d", LED_PLAINS);
			return 1;
		}
	}

	if (draw_circle_args.vplain->count>0) {
		vplain = draw_circle_args.vplain->ival[0];
		DEB( "vplain:%d", vplain);
		if (vplain<0 || vplain>=LED_PLAINS) {
			printf("value must be between 0 up to %d", LED_PLAINS);
			return 1;
		}
	}

	if (draw_circle_args.center_x->count>0) {
		centerx = draw_circle_args.center_x->ival[0];
		DEB( "center_x:%d", centerx);
		if (centerx<0||centerx>=LED_ROWS) {
			printf("value must be between 0 up to %d", LED_ROWS);
			return 1;
		}
	}

	if (draw_circle_args.center_y->count>0) {
		centery = draw_circle_args.center_y->ival[0];
		DEB( "centerx_y:%d", centery);
		if (centery<0||centery>=LED_COLUMNS) {
			printf("value must be between 0 up to %d", LED_COLUMNS);
			return 1;
		}
	}

	if (draw_circle_args.radius->count>0) {
		radius = draw_circle_args.radius->ival[0];
		DEB( "radius:%d", radius);
		if (radius<0||radius>=LED_COLUMNS/2) {
			printf("value must be between 0 up to %d", LED_COLUMNS/2);
			return 1;
		}
	}

	if (draw_circle_args.color->count>0) {
		parse_color(draw_circle_args.color->sval[0], &color);
		DEB( "color: %02x%02x%02x", color.red, color.green, color.blue);
	}

	if (radius<0) {
		printf("Need to set radius\n");
		return 1;
	}

	if (centerx<0) {
		printf("Need to set center X\n");
		return 1;
	}

	if (centery<0) {
		printf("Need to set center Y\n");
		return 1;
	}

	if (hplain >=0 && vplain>=0) {
		printf("both hplain & vplain defined\n");
		return 1;
	}

	if (hplain<0 && vplain<0) {
		printf("need to set hplain or vplain\n");
		return 1;
	}

	if (hplain>=0 && led_draw_circle(P_HORIZONTAL, hplain, centerx, centery, radius, &color) == EXIT_SUCCESS) {
		led_show();
		ESP_LOGI(TAG_CMD_LED, "draw circle on hplain:%d, cx:%d, cy:%d, r:%d with #%02x%02x%02x",
							  hplain, centerx, centery, radius, color.red, color.green, color.blue);
		return 0;
	}

	if (vplain>=0 && led_draw_circle(P_VERTICAL, vplain, centerx, centery, radius, &color) == EXIT_SUCCESS) {
		led_show();
		ESP_LOGI(TAG_CMD_LED, "draw circle on vplain:%d, cx:%d, cy:%d, r:%d with #%02x%02x%02x",
							  vplain, centerx, centery, radius, color.red, color.green, color.blue);
		return 0;
	}

	arg_print_errors(stderr, set_pixel_args.end, argv[0]);
	return 1;
}

static void register_draw_circle() {
	draw_circle_args.hplain = arg_int0("h", "hplain", "<plain>", "horizontal plain index");
	draw_circle_args.vplain = arg_int0("v", "vplain", "<plain>", "vertical plain index");
	draw_circle_args.center_x = arg_int0("x", "centerx", "<x>", "center x");
	draw_circle_args.center_y = arg_int0("y", "centery", "<y>", "center y");
	draw_circle_args.radius = arg_int0("r", "radius", "<radius>", "radius of circle");
	draw_circle_args.color = arg_rex0("o", "color", "#[0-9a-fA-F]+","#RGB color", REG_ICASE, "color");
	draw_circle_args.end = arg_end(2);
	const esp_console_cmd_t cmd = {
		.command = "drawcircle",
		.help = "Draw a circle .\n"
				" Example:\n"
				"   drawcircle --vplain 1 --centerx 3 --centery 2 --radius 3 --color #424242\n"
				"   drawcircle --hplain 1 -x 3 -y 2 -r 3 -o #424242\n"
				"   if -o is omitted it will use the default color. See setcolor command\n",
		.hint = NULL,
		.func = &draw_circle
	};

	ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}



static struct {
   struct arg_int *hplain;
   struct arg_int *vplain;
   struct arg_int *x0;
   struct arg_int *y0;
   struct arg_int *x1;
   struct arg_int *y1;
   struct arg_rex *color;
   struct arg_end *end;
} draw_line_args;

static int draw_line(int argc, char **argv) {
	int nerrors = arg_parse(argc, argv, (void**) &draw_line_args);
	DEB( "nerrors:%d", nerrors);

	if (nerrors) {
		arg_print_errors(stderr, draw_line_args.end, argv[0]);
		return 1;
	}

	int hplain=-1;
	int vplain=-1;
	int x0=-1;
	int y0=-1;
	int x1=-1;
	int y1=-1;

	struct led_color_t color = led_get_default_color();

	if (draw_line_args.hplain->count>0) {
		hplain = draw_line_args.hplain->ival[0];
		DEB( "hplain:%d", hplain);
		if (hplain<0 || hplain>=LED_PLAINS) {
			printf("value must be between 0 up to %d", LED_PLAINS);
			return 1;
		}
	}

	if (draw_line_args.vplain->count>0) {
		vplain = draw_line_args.vplain->ival[0];
		DEB( "vplain:%d", vplain);
		if (vplain<0 || vplain>=LED_PLAINS) {
			printf("value must be between 0 up to %d", LED_PLAINS);
			return 1;
		}
	}

	if (draw_line_args.x0->count>0) {
		x0 = draw_line_args.x0->ival[0];
		DEB( "x0:%d", x0);
		if (x0<0||x0>=LED_ROWS) {
			printf("value must be between 0 up to %d", LED_ROWS);
			return 1;
		}
	}

	if (draw_line_args.x1->count>0) {
		x1 = draw_line_args.x1->ival[0];
		DEB( "x1:%d", x1);
		if (x1<0||x1>=LED_ROWS) {
			printf("value must be between 0 up to %d", LED_ROWS);
			return 1;
		}
	}

	if (draw_line_args.y0->count>0) {
		y0 = draw_line_args.y0->ival[0];
		DEB( "y0:%d", y0);
		if (y0<0||y0>=LED_ROWS) {
			printf("value must be between 0 up to %d", LED_ROWS);
			return 1;
		}
	}

	if (draw_line_args.y1->count>0) {
		y1 = draw_line_args.y1->ival[0];
		DEB( "y1:%d", y1);
		if (y1<0||y1>=LED_ROWS) {
			printf("value must be between 0 up to %d", LED_ROWS);
			return 1;
		}
	}

	if (draw_line_args.color->count>0) {
		parse_color(draw_line_args.color->sval[0], &color);
		DEB( "color: %02x%02x%02x", color.red, color.green, color.blue);
	}

	if (x0<0) {
		printf("Need to set center X0\n");
		return 1;
	}

	if (x1<0) {
		printf("Need to set center X1\n");
		return 1;
	}

	if (y0<0) {
		printf("Need to set center Y0\n");
		return 1;
	}

	if (y1<0) {
		printf("Need to set center Y1\n");
		return 1;
	}

	if (hplain >=0 && vplain>=0) {
		printf("both hplain & vplain defined\n");
		return 1;
	}

	if (hplain<0 && vplain<0) {
		printf("need to set hplain or vplain\n");
		return 1;
	}

	if (hplain>=0 && led_draw_line(P_HORIZONTAL, hplain, x0, y0, x1, y1, &color) == EXIT_SUCCESS) {
		led_show();
		ESP_LOGI(TAG_CMD_LED, "draw line on hplain:%d, x0:%d, y0:%d, x1:%d y1:%d with #%02x%02x%02x",
							  hplain, x0, y0, x1, y1, color.red, color.green, color.blue);
		return 0;
	}

	if (vplain>=0 && led_draw_line(P_VERTICAL, vplain, x0, y0, x1, y1, &color) == EXIT_SUCCESS) {
		led_show();
		ESP_LOGI(TAG_CMD_LED, "draw line on vplain:%d, x0:%d, y0:%d, x1:%d, y1:%d with #%02x%02x%02x",
							  vplain, x0, y0, x1, y1, color.red, color.green, color.blue);
		return 0;
	}

	arg_print_errors(stderr, set_pixel_args.end, argv[0]);
	return 1;
}

static void register_draw_line() {
	draw_line_args.hplain = arg_int0("h", "hplain", "<plain>", "horizontal plain index");
	draw_line_args.vplain = arg_int0("v", "vplain", "<plain>", "vertical plain index");
	draw_line_args.x0 = arg_int0(NULL, "x0", "<x0>", "x0");
	draw_line_args.y0 = arg_int0(NULL, "y0", "<y0>", "y0");
	draw_line_args.x1 = arg_int0(NULL, "x1", "<x1>", "x1");
	draw_line_args.y1 = arg_int0(NULL, "y1", "<y1>", "y1");
	draw_line_args.color = arg_rex0("o", "color", "#[0-9a-fA-F]+","#RGB color", REG_ICASE, "color");
	draw_line_args.end = arg_end(2);
	const esp_console_cmd_t cmd = {
		.command = "drawline",
		.help = "Draw a line .\n"
				" Example:\n"
				"   drawline --vplain 1 --x0 0 --y0 0 --x1 7 --y1 7 --color #424242\n"
				"   if -o is omitted it will use the default color. See setcolor command\n",
		.hint = NULL,
		.func = &draw_line
	};

	ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}


void register_led_cmd() {
	register_get_current();
	register_set_current();
	register_get_default_color();
	register_set_default_color();
	register_set_pixel();
	register_set_display();
	register_draw_circle();
	register_draw_line();
}

