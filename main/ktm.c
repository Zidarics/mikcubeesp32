/*
    Copyright (C) zamek
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
 * ktm.c
 *
 *  Created on: Jun 17, 2020
 *      Author: zamek
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <strings.h>
#include <sys/queue.h>
#include "esp_system.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "led.h"
#include "led_strip.h"
#include "ktm.h"

#define FRAMES 200
#define FRAME_TIME_MS 200
#define NEW_ROCKETS_BY_FRAME 7

#define MAX_ROCKETS 8

#define MAX_PARTICLES 6
#define GRAVITY 0.4

#define DEBUG

#define TAG_KTM "KTM"

#ifdef DEBUG
#define KTM_DEB(fmt, ...)   ESP_LOGD(TAG_KTM, fmt, ##__VA_ARGS__)
#else
#define KTM_DEB(fmt, ...)
#endif

// Exploded rocket particles

struct explode_particle_t {
	int alive;
	float row;
	float column;
	float plain;
	float v_row;
	float v_column;
	float v_plain;
	float gravity;
	struct led_color_t *color;
};


static float randomf(float minf, float maxf) {
	float rnd = (float)(rand()%((int)maxf-(int)minf)*1000);
	rnd/=1000;
	rnd+=minf;
	return rnd;
}


static void init_rocket_particle(int plain, int row, int col, struct led_color_t *color, struct explode_particle_t *particle) {
	if (!particle)
		return;

	particle->color = color;
	particle->alive = 1;

	particle->plain = plain;
	particle->row = row;
	particle->column = col;
	particle->gravity = GRAVITY;
	particle->v_row=randomf(-2,2);
	particle->v_column=randomf(-1,2);
	particle->v_plain=randomf(-2,2);
}

static int update_particle(struct explode_particle_t *particle) {
	if (!(particle && particle->alive))
		return 0;

	particle->row+=particle->v_row;
	particle->column+=particle->v_column;
	particle->plain+=particle->v_plain;
	particle->v_column-=particle->gravity;

	if ((particle->plain<0||particle->plain>=LED_PLAINS)
		&& (particle->row<0||particle->row>=LED_ROWS)
		&& (particle->column<0||particle->column>=LED_COLUMNS))
		particle->alive=0;

	return particle->alive;
}

static void draw_particle(struct explode_particle_t *particle) {
	if(!(particle&&particle->alive))
		return;

	if (particle->row>=0 && particle->row<LED_COLUMNS && particle->plain>=0 && particle->plain <LED_ROWS)
		led_set_plain_pixel(P_HORIZONTAL, particle->plain, particle->row, particle->column, particle->color);
}


struct rocket_t {
	int row;
	int column;
	int plain;
	int exploded;
	int vertical_speed;
	struct led_color_t color;
	int size_of_particles;
	struct explode_particle_t particles[MAX_PARTICLES];
	TAILQ_ENTRY(rocket_t) next;
};


static struct rocket_t rockets[MAX_ROCKETS];

static struct {
	int initialized;
	TAILQ_HEAD(free_pool, rocket_t) free;
	TAILQ_HEAD(used_pool, rocket_t) used;
} rocket_pools;

static int init_rocket_pools() {
	TAILQ_INIT(&rocket_pools.free);
	TAILQ_INIT(&rocket_pools.used);


	for (int i=0;i<MAX_ROCKETS; ++i) {
		KTM_DEB("adding free:%d", i);
		TAILQ_INSERT_HEAD(&rocket_pools.free, &rockets[i], next);
	}

	rocket_pools.initialized = 1;
	return EXIT_SUCCESS;
}

static int pool_free_counts() {
	struct rocket_t *item;
	int c=0;
	TAILQ_FOREACH(item, &rocket_pools.free, next)
		++c;

	return c;
}

static void draw_ground() {
	struct led_color_t color;
	color.red=30;
	color.green=220;
	color.blue=30;

	for (int i=0;i<LED_ROWS;++i)
		for (int j=0;j<LED_COLUMNS;j++)
			led_set_plain_pixel(P_HORIZONTAL, 0, i, j, &color);
}

static void init_rocket(struct rocket_t *rocket, int row, int col) {
	if (!rocket)
		return;
	KTM_DEB("enter init_rocket, row:%d, col:%d", row, col);
	bzero(rocket, sizeof(struct rocket_t));
	rocket->plain = 0;
	rocket->row = row;
	rocket->column = col;
	rocket->vertical_speed=1;
	rocket->color.red = randomf(1, 255);
	rocket->color.green = randomf(1, 255);
	rocket->color.blue = randomf(1, 255);
}

static void add_rocket() {
	struct rocket_t *r = TAILQ_FIRST(&rocket_pools.free);
	if (!r) {
		ESP_LOGE(TAG_KTM, "free pool is too small");
		return;
	}

	TAILQ_REMOVE(&rocket_pools.free, r, next);
	init_rocket(r, randomf(2, 6), randomf(2,6));
	TAILQ_INSERT_TAIL(&rocket_pools.used, r, next);
}

static void remove_rocket(struct rocket_t *r) {
	if (!r)
		return;
	KTM_DEB("enter rocket remove");
	TAILQ_REMOVE(&rocket_pools.used, r, next);
	TAILQ_INSERT_HEAD(&rocket_pools.free, r, next);
}


static int rocket_particle_update(struct rocket_t *rocket) {
	if(!rocket)
		return 0;

	int valid = 0;
	for (int i=0;i<rocket->size_of_particles; ++i)
		if (update_particle(&rocket->particles[i]))
			++valid;

	if (valid)
		return 1;

	remove_rocket(rocket);
	return 0;
}

static void rocket_explode(struct rocket_t *rocket) {
	if (!rocket)
		return;

	rocket->exploded = 1;
	rocket->size_of_particles = randomf(MAX_PARTICLES-3 ,MAX_PARTICLES);
	for (int i=0; i<rocket->size_of_particles;++i)
		init_rocket_particle(rocket->row, rocket->column, rocket->plain, &rocket->color, &rocket->particles[i]);

}

static void rocket_update(struct rocket_t *rocket) {
	if (!rocket)
		return;

//	KTM_DEB("Enter rocket_update, plain:%d, row:%d, col:%d, exploded:%d",
//			rocket->plain, rocket->row, rocket->column, rocket->exploded);

	if (rocket->exploded && !rocket_particle_update(rocket)) {
		KTM_DEB("Rocket invalidated");
		return;
	}

	if (rocket->plain>=6) {
		KTM_DEB("rocket->plain>6:%d", rocket->plain);
		rocket_explode(rocket);
		return;
	}

	rocket->plain+=rocket->vertical_speed;
}

static void rocket_draw(struct rocket_t *rocket) {
	if (!rocket)
		return;

	if (rocket->exploded) {
		for (int i=0;i<rocket->size_of_particles;++i)
			draw_particle(&rocket->particles[i]);
	}
	else
		led_set_plain_pixel(P_HORIZONTAL, rocket->plain, rocket->row, rocket->column, &rocket->color);
}

void ktm() {
	srand(time(0));
	bzero(rockets, sizeof(rockets));

	init_rocket_pools();
	KTM_DEB("Free pool size:%d", pool_free_counts());

	add_rocket();
	for(int f=0;f<FRAMES;++f) {
		draw_ground();
		struct rocket_t *r;
		TAILQ_FOREACH(r, &rocket_pools.used, next) {
			rocket_update(r);
			rocket_draw(r);
		}

		led_show();

//		if (f%NEW_ROCKETS_BY_FRAME==0)
//			add_rocket();

		vTaskDelay(FRAME_TIME_MS/portTICK_RATE_MS);
	}
}

