/**
 * 
 */

#include <stdlib.h>
#include <math.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_system.h"
#include "hal/gpio_types.h"
#include "hal/rmt_types.h"
#include "esp_task.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "assert.h"
#include "font8x8_basic.h"

#include "led.h"

#undef DEBUG

#define LED_INFO(fmt, ...) ESP_LOGI(LED_TAG, fmt, ##__VA_ARGS__) 
#define LED_WARN(fmt, ...) ESP_LOGW(LED_TAG, fmt, ##__VA_ARGS__) 
#define LED_ERR(fmt, ...) ESP_LOGE(LED_TAG, fmt, ##__VA_ARGS__) 

#ifdef DEBUG
#define DEB(fmt, ...)   ESP_LOGD(LED_TAG, fmt, ##__VA_ARGS__)
#else
#define DEB(fmt, ...)
#endif

#define TASK_DEPTH 6144 + BT_TASK_EXTRA_STACK_SIZE


#define MIN_DISPLAY_TEXT_TIME_MS 100

static struct led_color_t led_strip_buf_1[LED_STRIP_LENGTH];
static struct led_color_t led_strip_buf_2[LED_STRIP_LENGTH];

static struct led_strip_t led_strip = {
    .rgb_led_type = RGB_LED_TYPE_WS2812,
    .rmt_channel=RMT_CHANNEL_1,
    .rmt_interrupt_num =  LED_STRIP_RMT_INTERRUPT_NUM,
    .gpio = GPIO_NUM_27,
    .led_strip_buf_1 = led_strip_buf_1,
    .led_strip_buf_2 = led_strip_buf_2,
    .led_strip_length = LED_STRIP_LENGTH
};

static struct {
    bool initialized;
    TaskHandle_t task;
} cube = {
    .initialized=0,
    .task = NULL
};

static volatile enum predefined_figures_t current_figure=PF_SINE;

static struct led_color_t current_color;

static volatile int display=1;

static void led_task(void *pvParameters); 

int led_init(){
    DEB("Enter led_init");
    led_strip.access_semaphore = xSemaphoreCreateBinary();
    
    cube.initialized=led_strip_init(&led_strip);
    if (!cube.initialized) {
        return false;
    }

    configASSERT(xTaskCreatePinnedToCore(led_task, "Led task", TASK_DEPTH, NULL, ESP_TASK_PRIO_MIN, &cube.task, 1));

    DEB("Led successflully initialized");
    return true;
}

int led_deinit(){
    DEB("Enter led_deinit");
    if (cube.task)
        vTaskDelete(cube.task);

    return true; //TODO
}

void led_set_display(int on) {
	display = on;
}

void led_show() {
    led_strip_show(&led_strip);
}

#define COLOR_STEP (4)

static void next_color(struct led_color_t *color) {

	color->red+=COLOR_STEP;
	if (color->red>=0xff) {
		color->red=0;
		return;
	}


	color->green+=COLOR_STEP;
	if (color->green>=0xff) {
		color->green=0;
		return;
	}

	color->blue =(color->blue+COLOR_STEP) % 0xff;
}

static inline int get_physical_index(int logical_index) {
	int index_in_plain = logical_index % LEDS_PER_PLAIN;
	int plain = logical_index/LEDS_PER_PLAIN;
	int col=index_in_plain/LED_COLUMNS;
	int row = index_in_plain%LED_ROWS;
	int plain_base = plain % 2 == 0   // even plain
			? plain*LEDS_PER_PLAIN + (LEDS_PER_PLAIN)-1
	        : plain*LEDS_PER_PLAIN; // odd

	if(plain%2==0) {
		return col%2==0
				? plain_base - col*LED_COLUMNS - LED_ROWS  + row + 1
				: plain_base - col*LED_COLUMNS - row;
	}
	return col%2==0
			 ? plain_base + col*LED_COLUMNS + LED_ROWS - row-1
		     : plain_base + col*LED_COLUMNS + row;
}

int led_set_absolute_pixel(uint16_t index, struct led_color_t *color) {
	if (!(color && index<LED_COLUMNS*LED_ROWS*LED_PLAINS))
		return EXIT_FAILURE;

	led_strip_set_pixel_color(&led_strip, get_physical_index(index), color);
	return EXIT_SUCCESS;
}

int led_set_plain_pixel(enum plain_t plain_type,  uint8_t plain_index, uint8_t row, uint8_t col, struct led_color_t *color) {
	DEB("Enter led_set_plain_pixel, type:%d, plain:%d, row:%d, col:%d, color:#%02x%02x%02x",
			plain_type, plain_index, row, col, color->red, color->green, color->blue);
    if(!color || plain_type >= P_UNKNOWN || plain_index >= LED_PLAINS || row >=LED_ROWS || col >= LED_COLUMNS) {
        ESP_LOGE(LED_TAG, "led_set_plain_pixel param error, plain_type:%d, plain_index:%d, row:%d, col:%d, color:%p", 
                          plain_type, plain_index, row, col, color);
        return EXIT_FAILURE;
    }

    switch (plain_type) {
    case P_HORIZONTAL :
        	DEB("mod:0, h: row:%d, col:%d, idx: %d", row, col, plain_index * (LED_ROWS*LED_COLUMNS)  + row*LED_ROWS + col);
        	led_set_absolute_pixel(plain_index * (LED_ROWS*LED_COLUMNS)  + row*LED_ROWS + col, color);
        return EXIT_SUCCESS;
    case P_VERTICAL :
        	led_set_absolute_pixel(row * LED_ROWS*LED_COLUMNS  + col*LED_COLUMNS + plain_index, color);

        return EXIT_SUCCESS;
    default:
        ESP_LOGE(LED_TAG, "Unknown plain_type: %d", plain_type);
        return EXIT_FAILURE;
    }
    return EXIT_FAILURE;
}

static void increment_plains(enum plain_t plainType) {
    DEB("Enter increment_plains, plainType:%d", plainType);
    struct led_color_t color;
    color.red = 0xff;
    color.green = 0;
    color.blue = 0;
    enum predefined_figures_t c = current_figure;
    while(display && (current_figure==PF_SHIFT_HORIZONTAL_PLAINS || current_figure == PF_SHIFT_VERTICAL_PLAINS)) {
        led_strip_clear(&led_strip);
       for (int p=0; p<LED_PLAINS; ++p) {
            for (int r=0;r<LED_ROWS; ++r) {
                for(int c=0; c<LED_COLUMNS; ++c)  {
                    led_set_plain_pixel(plainType, p, r, c, &color);
                }
            }
            led_strip_show(&led_strip);
            vTaskDelay(400/portTICK_PERIOD_MS);
        }
       if (!(display && (c==current_figure)))
    	   break;
    }
}

static void increment_leds() {
    DEB("Enter increment_leds");
    struct led_color_t color;
    color.red = 0xff;
    color.green = 0;
    color.blue = 0;
    while(display && current_figure==PF_ON_OFF_EACH_LED) {
        for (int i=0; i<LED_STRIP_LENGTH; ++i) {
        	led_set_absolute_pixel(i, &color);
            led_strip_show(&led_strip);
            vTaskDelay(40/portTICK_PERIOD_MS);
            if (!(display && (current_figure==PF_ON_OFF_EACH_LED)))
            	break;
        }
    }
}


static int render_letter(uint8_t plain, char *font, char c, struct led_color_t *color) {
    DEB("Enter render_letter, plain:%d, c:%c", plain, c);
    if (plain>=LED_PLAINS || !font || !color)
        return EXIT_FAILURE;

    char *p = font+c*8;
    for (int i=0;i<LED_ROWS;++i) {
        uint8_t r = *(p+LED_ROWS-1-i);
        for (int j=0; j<8; ++j) {
            if (r&(1<<j))
                led_set_plain_pixel(P_VERTICAL, plain, i, LED_COLUMNS-j-1, color);
        }
    }
    led_strip_show(&led_strip);
    return EXIT_SUCCESS;
}

static int display_text(char *text, int display_time_ms, struct led_color_t *color) {
    if (!(text && color && display_time_ms>MIN_DISPLAY_TEXT_TIME_MS))
        return EXIT_FAILURE;

    char c;
    while(display && (c=*text++)) {
        for (int i=0; i<LED_PLAINS; ++i) {
            render_letter(i, font8x8_basic, c, color);
            vTaskDelay(display_time_ms/portTICK_PERIOD_MS);
        }
        if (!(display && (current_figure==PF_SHOW_TEXT)))
        	break;
    }
    return EXIT_SUCCESS;
}

static inline void circle_point(enum plain_t plain_type, uint8_t plain_index, uint8_t x, int8_t y, struct led_color_t *color) {
	led_set_plain_pixel(plain_type, plain_index, x, y, color);
}


int led_draw_circle(enum plain_t plain_type, uint8_t plain_index, uint8_t origo_x, uint8_t origo_y, uint8_t radius, struct led_color_t *color) {
	DEB("Enter led_draw_circle, pt:%d, plain:%d, x:%d y:%d radius:%d", plain_type, plain_index, origo_x, origo_y, radius);

	if (plain_type<0|| plain_type>=P_UNKNOWN || plain_index>=LED_PLAINS || origo_x >= LED_ROWS || origo_y >= LED_COLUMNS ||
		 radius>=LED_ROWS*1.44) {
		DEB("Failure");
		return EXIT_FAILURE;
	}

	if (!color)
		color = &current_color;

	int f=1-radius;
	int ddfX = 0;
	int ddfY = 0;
	int x=0;
	int y=radius;

	circle_point(plain_type, plain_index, origo_x, origo_y+radius, color);
	circle_point(plain_type, plain_index, origo_x, origo_y-radius, color);
	circle_point(plain_type, plain_index, origo_x+radius, origo_y, color);
	circle_point(plain_type, plain_index, origo_x-radius, origo_y, color);

	while(x<y) {
		if (f>=0) {
			--y;
			ddfY+=2;
			f += ddfY;
		}
		++x;
		ddfX+=2;
		f=f+ddfX+1;

		circle_point(plain_type, plain_index, origo_x+x, origo_y+y, color);
		circle_point(plain_type, plain_index, origo_x-x, origo_y+y, color);

		circle_point(plain_type, plain_index, origo_x+x, origo_y-y, color);
		circle_point(plain_type, plain_index, origo_x-x, origo_y-y, color);

		circle_point(plain_type, plain_index, origo_x+y, origo_y+x, color);
		circle_point(plain_type, plain_index, origo_x-y, origo_y+x, color);

		circle_point(plain_type, plain_index, origo_x+y, origo_y-x, color);
		circle_point(plain_type, plain_index, origo_x-y, origo_y-x, color);

	}

	return EXIT_SUCCESS;
}


static void plot_line_low(enum plain_t plain_type, uint8_t plain_index, uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, struct led_color_t *color) {
	int dx = x1-x0;
	int dy = y1-y0;
	int yi=1;
	if (dy<0) {
		yi=-1;
		dy=-dy;
	}
	int d=2*dy-dx;
	int y=y0;
	for (int x=x0;x<=x1;++x) {
		led_set_plain_pixel(plain_type, plain_index, x, y, color);
		if (d>0) {
			y+=yi;
			d=d-2*dx;
		}
		d=d+2*dy;
	}
}

static void plot_line_high(enum plain_t plain_type, uint8_t plain_index, uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, struct led_color_t *color) {
	int dx = x1-x0;
	int dy = y1-y0;
	int xi=1;
	if (dx<0) {
		xi=-1;
		dx=-dx;
	}
	int d=2*dx-dy;
	int x=x0;
	for (int y=y0;y<=y1;++y) {
		led_set_plain_pixel(plain_type, plain_index, x, y, color);
		if (d>0) {
			x+=xi;
			d=d-2*dy;
		}
		d=d+2*dx;
	}
}


int led_draw_line(enum plain_t plain_type, uint8_t plain_index, uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, struct led_color_t *color) {
	DEB("Enter led_draw_line, pt:%d, plain:%d, x0:%d, y0:%d, x1:%d, y1:%d\n",
			plain_type, plain_index, x0, y0, x1, y1);
	if (plain_type<0||plain_type>=P_UNKNOWN || plain_index>=LED_PLAINS || x0 >= LED_ROWS ||y0 >= LED_COLUMNS ||
			x1>= LED_ROWS || y1>= LED_COLUMNS) {
		DEB("Failure\n");
		return EXIT_FAILURE;
	}

	if (!color)
		color = &current_color;

	if (abs(y1-y0)< abs(x1-x0)) {
		if (x0>x1)
			plot_line_low(plain_type,plain_index,x1,y1,x0,y0,color);
		else
			plot_line_low(plain_type,plain_index,x0,y0,x1,y1,color);
	}
	else {
		if (y0>y1)
			plot_line_high(plain_type,plain_index, x1,y1,x0,y0,color);
		else
			plot_line_high(plain_type,plain_index, x0,y0,x1,y1,color);
	}
	return EXIT_SUCCESS;
}

#define FRAME_TIME_MS 90

static void draw_lines() {
	struct led_color_t color;
	color.red = 0x10;
	color.green = 0;
	color.blue = 0;
	while(display && current_figure == PF_LINES) {
		for (int r=0;r<LED_ROWS;r++) {
			for (int i=0;i<LED_PLAINS;++i) {
				led_draw_line(P_VERTICAL, i, 0, r, 7, 7-r, &color);
			}
			led_show();
			vTaskDelay(FRAME_TIME_MS/portTICK_PERIOD_MS);
		}
		for (int r=0;r<LED_ROWS;r++) {
			for (int i=0;i<LED_PLAINS;++i) {
				led_draw_line(P_VERTICAL, i, 7-r, 0, r, 7, &color);
			}
			led_show();
			vTaskDelay(FRAME_TIME_MS/portTICK_PERIOD_MS);
		}

		for (int r=0;r<LED_ROWS;r++) {
			for (int i=0;i<LED_PLAINS;++i) {
				led_draw_line(P_HORIZONTAL, i, 0, r, 7, 7-r, &color);
			}
			led_show();
			vTaskDelay(FRAME_TIME_MS/portTICK_PERIOD_MS);
		}
		for (int r=0;r<LED_ROWS;r++) {
			for (int i=0;i<LED_PLAINS;++i) {
				led_draw_line(P_HORIZONTAL, i, 7-r, 0, r, 7, &color);
			}
			led_show();
			vTaskDelay(FRAME_TIME_MS/portTICK_PERIOD_MS);
		}

		color.red = (color.red + 0x10) % 0xff;
		color.green = (color.green+0x10) % 0xff;
		color.blue = (color.blue+0x10) % 0xff;
	}
}

static void display_circle() {
	struct led_color_t color;
	color.red = 0x10;
	color.green = 0;
	color.blue = 0;
	while(display && current_figure == PF_CIRCLE) {
		for (int i=0;i<LED_PLAINS;++i) {
			for (int j=3;j>=0;--j) {
				led_draw_circle(P_HORIZONTAL, i, 3, 4, j, &color);
				led_show();
				vTaskDelay(FRAME_TIME_MS/portTICK_PERIOD_MS);
			}
			for (int j=0;j<=3;++j) {
				led_draw_circle(P_HORIZONTAL, i, 3, 4, j, &color);
				led_show();
				vTaskDelay(FRAME_TIME_MS/portTICK_PERIOD_MS);
			}
		}
		color.red = (color.red + 0x10) % 0xff;
		color.green = (color.green+0x10) % 0xff;
		color.blue = (color.blue+0x10) % 0xff;
	}
}

static inline double map(double in, double in_min, double in_max, double out_min, double out_max) {
	return (in - in_min)/(in_max-in_min) * (out_max-out_min) + out_min;
}

static const double PI=3.14;
static const double PHASE_STEP = PI/8;

static void draw_sine() {
	struct led_color_t color;
	color.red = 0x10;
	color.green = 0;
	color.blue = 0;
	while(display && current_figure == PF_SINE) {
		double phase=0;
		while (phase<2*PI) {

			for (int x=0;x<LED_ROWS;++x) {
				for (int y=0;y<LED_COLUMNS;++y) {
					float z = sin(phase+sqrt(pow(map(x,0,LED_ROWS-1, -PI, PI),2) + pow(map(y,0,LED_COLUMNS-1, -PI,PI), 2)));
					z=round(map(z,-1,1,0,LED_ROWS-1));
					DEB("x:%d, y:%d, z:%f", x, y, z);
					led_set_plain_pixel(P_HORIZONTAL, z, x, y, &color);
				}
			}
			led_show();
			vTaskDelay(FRAME_TIME_MS/portTICK_PERIOD_MS);

			phase += PHASE_STEP;
		}
		next_color(&color);
	}
}

static void draw_all() {
	struct led_color_t color;
	color.red = 0x10;
	color.green = 0;
	color.blue = 0;
	while(display && current_figure == PF_ALL) {
		DEB(".");

	    for (int i=0;i<LEDS_ALL;++i)
			led_set_absolute_pixel(i, &color);

	    led_show();
		next_color(&color);
		vTaskDelay(FRAME_TIME_MS/portTICK_PERIOD_MS);
	}
}

static void led_task(void *pvParameters) {
    DEB("Enter led_task, current_figure:%d", current_figure);
    
    led_strip_clear(&led_strip);

    struct led_color_t color;
    color.red = 0x80;
    color.green = 0xff;
    color.blue = 0x80;
    int dflag=0;

    for (;;) {
    	if (!display) {
    		if (dflag++==0) {
    			led_strip_clear(&led_strip);
    			led_strip_show(&led_strip);
    		}
    		vTaskDelay(100/portTICK_PERIOD_MS);
    		continue;
    	}
    	else
    		dflag=0;

        switch (current_figure) {
        case PF_ON_OFF_EACH_LED:
            increment_leds();
            break;

        case PF_SHIFT_HORIZONTAL_PLAINS:
            increment_plains(P_HORIZONTAL);
            break;
        
        case PF_SHIFT_VERTICAL_PLAINS : 
            increment_plains(P_VERTICAL);
            break;

        case PF_SHOW_TEXT : 
            display_text("PTE-MIK", 200, &color);
            break;

        case PF_CIRCLE :
        	display_circle();
        	break;

        case PF_LINES :
        	draw_lines();
        	break;

        case PF_SINE :
        	draw_sine();
        	break;

        case PF_ALL :
        	draw_all();
        	break;

        default:
        	ESP_LOGE(LED_TAG, "Unknown figure:%d", current_figure);
            break;
        }
   }
}

int led_set_current_figure(enum predefined_figures_t figure) {
    if (!(figure>=0 && figure < PF_UNKNOWN))
        return EXIT_FAILURE;

    if (figure!=current_figure)
        current_figure=figure;
        
    return EXIT_SUCCESS;
}

enum predefined_figures_t led_get_current_figure() {
    return current_figure;
}

int led_get_max_plain() {
    return LED_PLAINS;
}

int led_get_max_row() {
    return LED_ROWS;
}

int led_get_max_column() {
    return LED_COLUMNS;
}

void led_set_default_color(struct led_color_t *color) {
	current_color.red = color->red;
	current_color.green = color->green;
	current_color.blue = color->blue;
}

struct led_color_t led_get_default_color() {
	return current_color;
}

