#ifndef __LED_H__

#include "led_strip.h"
#include <stdint.h>

#define LED_STRIP_RMT_INTERRUPT_NUM 19U
#define LED_STRIP_LENGTH 512U
#define LED_TAG "Led control"

#define LED_COMMAND_DEFAULT_COUNTS 10
#define LED_PLAINS (8)
#define LED_ROWS (8)
#define LED_COLUMNS (8)
#define LEDS_PER_PLAIN ((LED_ROWS)*(LED_COLUMNS))
#define LEDS_ALL ((LED_ROWS)*(LED_COLUMNS) * (LED_PLAINS))

enum predefined_figures_t {
    PF_ON_OFF_EACH_LED, 
    PF_SHIFT_HORIZONTAL_PLAINS,
    PF_SHIFT_VERTICAL_PLAINS,
    PF_SHOW_TEXT,
	PF_CIRCLE,
	PF_LINES,
	PF_SINE,
	PF_ALL,
    PF_UNKNOWN
};

enum plain_t {
    P_HORIZONTAL,
    P_VERTICAL,
    P_UNKNOWN
};


int led_init();

int led_deinit();

void led_set_display(int on);

void led_show();

int led_set_plain_pixel(enum plain_t plain_type,  uint8_t plain_index, uint8_t row, uint8_t col, struct led_color_t *color);

int led_set_absolute_pixel(uint16_t index, struct led_color_t *color);

int led_set_current_figure(enum predefined_figures_t figure);

enum predefined_figures_t led_get_current_figure();

int led_get_max_plain();

int led_get_max_row();

int led_get_max_column();

void led_set_default_color(struct led_color_t *color);

struct led_color_t led_get_default_color();

int led_draw_circle(enum plain_t plain_type, uint8_t plain_index, uint8_t origo_x, uint8_t origo_y, uint8_t radius, struct led_color_t *color);

int led_draw_line(enum plain_t plain_type, uint8_t plain_index, uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, struct led_color_t *color);

#endif
